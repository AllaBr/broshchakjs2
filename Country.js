const {City} = require('./City')

class Country {
    constructor(name) {
        this.name = name
        this.cities = []
        }
    AddCity(cityName)
    {
        let city = new City(cityName)
        this.cities.push(city)
    }
    DeleteCity (cityName)
    {
        const city = (c) => c.name === cityName
        let idx = this.cities.findIndex(city)
        if (idx >= 0)
            this.cities.splice(idx, 1)
    }

    // mySort(a, b){
    //     if(a.temperature < b.temperature)
    //         return 1
    //     if(a.temperature > b.temperature)
    //         return -1
    //     return 0
    // }
    SortCitiesByTemperature(){
        this.cities.sort()
    }

}

module.exports = {Country}