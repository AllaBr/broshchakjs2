const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Forecast{
    constructor(day, temperature, wind) {
        this.day = day
        this.temperature = temperature
        this.wind = wind
    }
    toString(){
        return `day: ${this.day}; temperature: ${this.temperature}; wind ${this.wind}`
    }
}

class Weather {
    constructor() {
        this.temperature = ''
        this.wind = ''
        this.date = Date.now()
        this.forecastArray = []
    }
   async  setWeather(cityName){
       await axios.get(weatherUrl + cityName).then((response) => {
           this.temperature = response.data.temperature
           this.wind = response.data.wind
       }).catch((error) => {return error.message})

    }
    async setForecast(cityName){
       await axios.get(weatherUrl + cityName).then((response) => {
             for (let i = 0; i < response.data.forecast.length; i++) {
              this.forecastArray.push(new Forecast(response.data.forecast[i].day, response.data.forecast[i].temperature, response.data.forecast[i].wind))
            }
        }).catch((error) => {return error.message})
    }
}

module.exports = {Weather}